#!/usr/bin/sh

# Prepare bin directory
mkdir -p bin

# Build producer
echo "> > > Building src/producer/producer.cpp < < <"
mkdir -p src/producer/cmake-build
cd src/producer/cmake-build
cmake ..
cmake --build .
mv producer ../../../bin
cd ../../..
rm -rf src/producer/cmake-build

# Build processor
echo "> > > Building src/processor/processor.cpp < < <"
mkdir -p src/processor/cmake-build
cd src/processor/cmake-build
cmake ..
cmake --build .
mv processor ../../../bin
cd ../../..
rm -rf src/processor/cmake-build

# Build synthesizer
echo "> > > Building src/synthesizer/synthesizer.cpp < < <"
g++ src/synthesizer/synthesizer.cpp -I src/lib -Llib -lGamma -lportaudio -lrt -o bin/synthesizer -std=c++17
echo "> > > Building src/synthesizer/synth_interactive_controller.cpp < < <"
g++ src/synthesizer/synth_interactive_controller.cpp -lrt -o bin/synth_interactive_controller -std=c++17

# Build manager
echo "> > > Building src/manager/manager.cpp < < <"
g++ src/manager/manager.cpp -lrt -lpthread -o bin/manager -std=c++17
# -Wall -Wextra -pedantic 
