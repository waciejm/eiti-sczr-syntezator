#!/usr/bin/sh

# Prepare log directory
mkdir -p log

# Install dependancies
sudo apt update
sudo apt upgrade
sudo apt install gcc g++ make cmake portaudio19-dev openexr libboost-all-dev python3 libopencv-dev
