# EiTI-SCZR-Syntezator

## Ubuntu 20.04 (lub podobne)
### Budowanie
```sh
./setup.sh
./build.sh
```
### Uruchomienie
```sh
./run.sh
#./run.sh --preview on     <- wlaczenie podgladu obrazu
#./run.sh --camera 0       <- wybarnie id kamery, warto sprobowac kilka pierwszych cyfr
```
### Możliwe parametry uruchomienia
```sh
./run.sh --help
```
### Uruchomienie testów
UWAGA: Po zakończeniu każdego testu (koniec dźwięku) trzeba ręcznie go zakończyć (enter w terminalu)
```
./test.sh
```

