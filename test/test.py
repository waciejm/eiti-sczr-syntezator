import os

class TestCase(object):

    def __init__(self, width, height, nprocessors, sched_prod, sched_proc, sched_synth):
        self.name = str(width) + "x" + str(height) + "@" + str(nprocessors) + "_" + sched_prod + "_" + sched_proc + "_" + sched_synth
        self.width = width
        self.height = height
        self.nprocessors = nprocessors
        self.sched_prod = sched_prod
        self.sched_proc = sched_proc
        self.sched_synth = sched_synth

    def run(self):
        command = "./run.sh --width " + str(self.width) + " --height " + str(self.height) + " --nprocessors " + str(self.nprocessors)
        command += " --file ./test/test_" + str(self.width) + "x" + str(self.height) + ".mp4"
        if (self.sched_prod != ""):
            command += " --sched-producer " + self.sched_prod
        if (self.sched_proc != ""):
            command += " --sched-processor " + self.sched_proc
        if (self.sched_synth != ""):
            command += " --sched-synthesizer " + self.sched_synth
        
        os.system("rm -rf ./log/*")
        print(command)
        os.system(command)

        self.collect_results()


    def collect_results(self):
        log_producer = open("log/log_producer").readlines()
        log_processor = open("log/log_processor").readlines()
        log_synthesizer = open("log/log_synthesizer").readlines()

        frames = []
        sent_frames = []
        played_frames = []

        for line in log_producer:
            sline = line.split(" ");
            if len(sline) == 3 and sline[1] == "grabbed":
                frames.append((int(sline[0]), int(sline[2])))
            if len(sline) == 3 and sline[1] == "sent":
                sent_frames.append((int(sline[0]), int(sline[2])))
        
        for line in log_synthesizer:
            sline = line.split(" ")
            if len(sline) == 3 and sline[1] == "next_pack":
                played_frames.append((int(sline[0]), int(sline[2])))

        sum_fps = 0
        n_fps = len(frames) - 1
        for i in range(len(frames) - 1):
            sum_fps += 1000000.0 / (frames[i+1][1] - frames[i][1])
        avg_fps = sum_fps / n_fps
        
        sent_rate = len(sent_frames) / len(frames)

        sum_process_time = 0
        n_process_time = 0
        for i in range(min(len(sent_frames), len(played_frames))):
            sum_process_time += played_frames[i][1] - sent_frames[i][1]
            n_process_time += 1
        avg_process_time = sum_process_time / n_process_time
    
        self.write_results(avg_fps, sent_rate, avg_process_time)

    def write_results(self, avg_fps, sent_rate, avg_process_time):
        file = open("test/" + self.name, "w")
        file.write(str(avg_fps) + ", " + str(sent_rate) + ", " + str(avg_process_time))


def generate_cases():
    cases = []

    resolutions = [(640, 360), (1280, 720), (1920, 1080)]
    for (width, height) in resolutions:
        cases.append(TestCase(width, height, 1, "", "", ""))

    for n in range(1, 4):
        cases.append(TestCase(1920, 1080, n, "", "", ""))

    schedules = [("rr", "rr", "rr"), ("fifo", "fifo", "fifo")]
    for (sched_prod, sched_proc, synth_process) in schedules:
        cases.append(TestCase(1920, 1080, 1, sched_prod, sched_proc, synth_process))
    
    return cases


def run_cases():
    cases = generate_cases()
    for case in cases:
        case.run()


if __name__ == "__main__":
    run_cases()