#include <iostream>
#include <cassert>
#include <chrono>
#include <fstream>
#include <time.h>

#include <opencv2/opencv.hpp>

#include "image_shared_buffer.hpp"

// Klasa producent - proces A
class Producer {
public:
    Producer() : log("log/log_producer", std::ofstream::app) {};
    
    void run() {
        // Prametry dzialania procesu
        int video_file, buffers, width, height, camera_id;
        std::string video_path;
        // Pobierz parametry ze strumienia (od zarzadcy - procesu D)
        std::cin >> video_file >> buffers >> width >> height;
        if (video_file) {
            std::cin >> video_path;
        } else {
            std::cin >> camera_id;
        }
        std::cin.ignore(10000, '\n');
        // Stworz dzielony bufor
        ImageSharedBuffer* isb = ImageSharedBuffer::create(buffers, width * height * 3);

        // Otworz poloczenie ze zrodlem obrazow
        cv::VideoCapture capture;
        if (video_file) {
            capture.open(video_path);
            capture.set(cv::CAP_PROP_FPS, 30);
        } else {
            capture.open(camera_id);
        }
        // Ustaw rozdzielczosc obrazu z kamery
        capture.set(cv::CAP_PROP_FRAME_WIDTH, width);
        capture.set(cv::CAP_PROP_FRAME_HEIGHT, height);

        // Poinformuj o rozpoczeciu pracy
        log_start();

        cv::Mat image;
        while (std::cin && capture.isOpened()) {

            // Pobierz obecny czas
            auto now = std::chrono::steady_clock::now();
            // Zlap kolejny obraz
            capture.grab();
            // Poinformuj o zlapaniu
            log_grabbed();

            // Sprawdz czy jest miejsce na obraz
            if (!isb->has_space()) {
                // Poinformuj o brakui miejsca
                log_skipped();
            } else {
                // Poinformuj o wysylaniu obrazu
                log_sent();
                // Pobierz obraz ze zrodla
                capture.retrieve(image);
                // Wyslij obraz
                bool sent = isb->insert_image((char*)image.data);
                assert(sent);
            }

            // Spij aby utrzymac maksymalna ilosc klatek na sekunde = 30
            // Potrzebne zeby pliki video były poprawnie odtwarzane
            std::this_thread::sleep_until(now + std::chrono::milliseconds(33));
        }

        // Poinformuj o zakonczeniu pracy
        log_exit();
        // Zwolnij zrodlo obrazu
        capture.release();
        // Usun dzielony bufor
        ImageSharedBuffer::unlink();
    }

private:
    int64_t index = 0;
    std::ofstream log;

    // Obecny czas
    std::int64_t get_time() {
        return std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now().time_since_epoch()
        ).count();
    }

    // Zapisz rozpoczecie pracy do logu
    void log_start() {
        log << "start " << get_time() << std::endl;
    }

    // Zapisz zlapanie obrazu do logu
    void log_grabbed() {
        log << index << " grabbed " << get_time() << std::endl;
    }

    // Zapisz pomieniecie obrazu do logu
    void log_skipped() {
        log << index << " skipped " << get_time() << std::endl;
        ++index;
    }

    // Zapisz wyslanie obrazu do logu
    void log_sent() {
        log << index << " sent " << get_time() << std::endl;
        ++index;
    }

    // Zapisz zakonczenie pracy do logu
    void log_exit() {
        log << "exit " << get_time() << std::endl;
    }
};


int main() {
    Producer().run();
    return 0;
}
