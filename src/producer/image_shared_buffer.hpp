#pragma once

#include <iostream>
#include <mutex>
#include <condition_variable>
#include <cstring>
#include <new>
#include <chrono>
#include <atomic>
#include <thread>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

// Nazwa dzielonej pamieci
const char* SHM_NAME = "/image_shared_buffer";

// Klasa pozwalajaca na dostep do dzielonego bufora obrazow
class ImageSharedBuffer {
public:

    // Czy jest miejsce na kolejny obraz
    bool has_space() {
        return newest_image - oldest_image < max_images;
    }

    // Czy jest dostępny obraz do wzięcia
    bool has_available() {
        return newest_image > newest_grabbed;
    }

    // Czy mozna zwolnic obraz o danym indeksie
    bool ready_to_drop(int64_t index) {
        return index == oldest_image + 1;
    }

    // Dodaj obraz do bufora
    bool insert_image(char* data) {
        bool sent = false;
        {
            if (has_space()) {
                ++newest_image;
                char* ptr = image_data(newest_image);
                std::memcpy(ptr, data, image_size);
                sent = true;
            }
        }
        return sent;
    }

    // Wez obraz z bufora
    char* grab_image(int64_t* index) {
        while (true) {
            {
                std::lock_guard<std::mutex> lg(mutex);
                if (has_available()) {
                    ++newest_grabbed;
                    *index = newest_grabbed;
                    return image_data(newest_grabbed);
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    // Zwolnij obraz z bufora
    void drop_image(int64_t index) {
        while (!ready_to_drop(index)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        ++oldest_image;
    }

    // Stworz dzielony bufor
    static ImageSharedBuffer* create(size_t max_images, size_t image_size) {
        int shm_file = shm_open(SHM_NAME, O_RDWR | O_CREAT, S_IRWXU);
        if (shm_file < 0) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (create): Failed to open shm - ") + strerror(err) + '\n');
            std::cerr << msg;
            return nullptr;
        }
        std::string msg = std::move(std::string("image_shared_buffer (create): Opened shm - ") + SHM_NAME + '\n');
        std::cerr << msg;

        size_t min_data_size = sizeof(ImageSharedBuffer) + image_size * max_images;
        int ferr = ftruncate(shm_file, min_data_size);
        if (ferr < 0) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (create): Failed to ftruncate shm - ") + strerror(err) + '\n');
            std::cerr << msg;
            close(shm_file);
            shm_unlink(SHM_NAME);
        }
        msg = std::move(std::string("image_shared_buffer (create): Ftruncated shm - ") + std::to_string(min_data_size) + '\n');
        std::cerr << msg;

        void* ptr = mmap(nullptr, min_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_file, 0);
        if (ptr == MAP_FAILED) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (create): Failed to mmap - ") + strerror(err) + '\n');
            std::cerr << msg;
            close(shm_file);
            shm_unlink(SHM_NAME);
        }
        msg = std::move(std::string("image_shared_buffer (create): Mmapped - ") + std::to_string((long long)ptr) + '\n');
        std::cerr << msg;
        close(shm_file);

        
        ImageSharedBuffer* isb = new(ptr) ImageSharedBuffer(max_images, image_size);

        return isb;
    }

    // Dolacz do istniejacego dzielonego bufora
    static ImageSharedBuffer* access(size_t image_size, size_t max_images) {
        int shm_file = shm_open(SHM_NAME, O_RDWR, S_IRWXU);
        if (shm_file < 0) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (access): Failed to open shm - ") + strerror(err) + '\n');
            std::cerr << msg;
            return nullptr;
        }
        std::string msg = std::move(std::string("image_shared_buffer (access): Opened shm - ") + SHM_NAME + '\n');
        std::cerr << msg;

        size_t min_data_size = sizeof(ImageSharedBuffer) + image_size * max_images;
        int ferr = ftruncate(shm_file, min_data_size);
        if (ferr < 0) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (access): Failed to ftruncate shm - ") + strerror(err) + '\n');
            std::cerr << msg;
            close(shm_file);
            shm_unlink(SHM_NAME);
        }
        msg = std::move(std::string("image_shared_buffer (access): Ftruncated shm - ") + std::to_string(min_data_size) + '\n');
        std::cerr << msg;

        void* ptr = mmap(nullptr, min_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_file, 0);
        if (ptr == MAP_FAILED) {
            int err = errno;
            std::string msg = std::move(std::string("image_shared_buffer (access): Failed to mmap - ") + strerror(err) + '\n');
            std::cerr << msg;
            close(shm_file);
            shm_unlink(SHM_NAME);
        }
        msg = std::move(std::string("image_shared_buffer (access): Mmapped - ") + std::to_string((long long)ptr) + '\n');
        std::cerr << msg;
        close(shm_file);

        return (ImageSharedBuffer*)ptr;
    }

    // Zwolnij dzielona pamiec
    static void unlink() {
        shm_unlink(SHM_NAME);
    }

private:
    // Mutex synchronizujacy dostep procesow B do pamieci
    std::mutex mutex;
    // Najstarszy obraz w buforze ktory nie zostal zwolniony
    std::atomic<int64_t> oldest_image;
    // Najnowszy pobrany obraz w buforze 
    std::atomic<int64_t> newest_grabbed;
    // Najnowszy umieszczony obraz w buforze
    std::atomic<int64_t> newest_image;
    // Rozmiar bufora
    size_t max_images;
    // Rozmiar jednego obrazu w buforze
    size_t image_size;

    ImageSharedBuffer(size_t max_i, size_t i_size)
    : mutex()
    , oldest_image(-1)
    , newest_grabbed(-1)
    , newest_image(-1)
    , max_images(max_i)
    , image_size(i_size)
    {}

    // Wskaznik na poczatek danych w dzielonym buforze
    char* data_pointer() {
        return (char*)this + sizeof(ImageSharedBuffer);
    }

    // Wskaznik na obraz w buforze o podanym indeksie
    char* image_data(int64_t index) {
        size_t offset = (index % max_images) * image_size;
        return data_pointer() + offset;
    }

};
