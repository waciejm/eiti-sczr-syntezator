#include <iostream>
#include <fstream>
#include <cmath>

#include <opencv2/opencv.hpp>

#include "../producer/image_shared_buffer.hpp"
#include "../synthesizer/synth_mq_controller.hpp"

// Klasa procesor - proces B
class Processor {
public:

    Processor() : log("log/log_processor", std::ofstream::app) {};

    // Uruchom procesor
    void run() {
        // Pobierz konfiguracje ze strumienia (od procesu D)
        int buffers, width, height;
        std::cin >> buffers >> width >> height >> preview;
        std::cin.ignore(10000, '\n');
        // Zdobadz dostep do dzielonego bufora
        ImageSharedBuffer* isb = ImageSharedBuffer::access(buffers, width * height * 3);
        // Zdobadz dostep do kolejki komunikatow
        SynthMqController smqc;

        // Stworz obraz do ktorego beda kopiowane dane obrazow z bufora
        cv::Mat image;
        image.create(cv::Size(width, height), CV_8UC3);
        // Stworz okno do podgladu jesli uzytkownik poprosil
        if (preview) cv::namedWindow("Camera preview");

        // Poinformuj o rozpoczeciu pracy
        log_start();

        while (std::cin) {
            int64_t index;
            
            // Zlap obraz z dzielonego bufora
            char* data = isb->grab_image(&index);
            // Poinformuj o zlapaniu obrazka
            log_grabbed();

            // Przetworz obraz
            std::memcpy(image.data, data, width * height * 3);
            process_image(image, smqc);

            // Zwolnij zlapany obraz
            isb->drop_image(index);
            // Poinformuj o zwolnieniu obrazka
            log_dropped();

            // Poczekaj jesli jest otwarte okno
            if (preview) {
                cv::waitKey(25);
            }
        }

        // Poinformuj o zakonczeniu pracy
        log_exit();

        // Zamknij okna jesli istnieja
        cv::destroyAllWindows();
    }

private:
    int64_t index = 0;
    std::ofstream log;
    int preview = 0;

    // Przetworz obraz
    // Przygotuj, przefiltruj, znajdz kola, wyslij punkty sterujace
    void process_image(cv::Mat& image, SynthMqController& smqc) {
        preprocess_image(image);
        cv::Mat filtered_image = filter_image(image);
        std::vector<cv::Vec3f> circles = find_circles(filtered_image);
        send_points(circles, image.cols, image.rows, smqc);
 
        // Wyswietl obraz z narysowanymi znalezionymi kolami, jest uzytkownik poprosil
        if (preview) {
            for (auto c : circles) {
                cv::circle(image, cv::Point(c[0], c[1]), c[2], cv::Scalar(255, 0, 0), 5);
            }
            cv::imshow("Camera preview", image);
        }
    }

    // Przygotuj obraz do filtorwania
    void preprocess_image(cv::Mat& image) {
        cv::medianBlur(image, image, 5);
    }

    // Przefiltruj obraz, zwroc maske
    cv::Mat filter_image(cv::Mat& image) {
        cv::Mat filtered;
        cv::Mat filtered2;
        // Przekonwertuj obraz z BGR na HSV
        cv::cvtColor(image, filtered, cv::COLOR_BGR2HSV);
        // Pobierz maski pikseli w podanym zakresie
        cv::inRange(filtered, cv::Scalar(0, 200, 75), cv::Scalar(15, 255, 240), filtered2);
        cv::inRange(filtered, cv::Scalar(160, 200, 75), cv::Scalar(179, 255, 240), filtered);
        // Polacz dwie maski
        cv::addWeighted(filtered, 1.0, filtered2, 1.0, 0.0, filtered);
        // Rozmyj maske
        cv::GaussianBlur(filtered, filtered, cv::Size(9, 9), 2, 2);
        return std::move(filtered);
    }

    // Znajdz kola na masce obrazu
    std::vector<cv::Vec3f> find_circles(cv::Mat& image) {
        std::vector<cv::Vec3f> circles;
        cv::HoughCircles(image, circles, cv::HOUGH_GRADIENT, 1, image.rows / 8, 100, 15, image.rows / 40, image.rows / 2);
        return std::move(circles);
    }

    // Wyslij punkty kontrolne ze znalezionych punktow na obrazie
    void send_points(std::vector<cv::Vec3f>& points, int iwidth, int iheight, SynthMqController& smqc) {       
        std::vector<SynthControlPack> scp;
        for (auto c : points) {
            int freq = scale_frequency(1.0 - ((float)c[0] / iwidth));
            float vol = scale_volume(1.0 - ((float)c[1] / iheight));
            scp.emplace_back(freq, vol);
        }
        smqc.send_control_packs(scp.data(), scp.size());
    }

    // Przeskaluj znormalizowana pozycje x do czestotliwosci
    int scale_frequency(float x_norm) {
        return (int)(100.0f + 440.0f * std::pow(x_norm * 2.0f, 2.0f));
    }

    // Przeskaluj znormalizowana pozycje y do glosnosci
    float scale_volume(float y_norm) {
        return 0.2 * std::sqrt(y_norm);
    }

    // Obecny czas
    std::int64_t get_time() {
        return std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now().time_since_epoch()
        ).count();
    }

    // Zapisz rozpoczecie pracy do logu
    void log_start() {
        log << "start " << get_time() << std::endl;
    }

    // Zapisz zlapanie obrazu do logu
    void log_grabbed() {
        log << index << " grabbed " << get_time() << std::endl;
    }

    // Zapisz zwolnienie obrazu do logu
    void log_dropped() {
        log << index << " dropped " << get_time() << std::endl;
        ++index;
    }

    // Zapisz zakonczenie pracy do logu
    void log_exit() {
        log << "exit " << get_time() << std::endl;
    }
};

int main() {
    Processor().run();
    return 0;
}
