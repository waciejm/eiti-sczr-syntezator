#include <string>

#include "synth_mq_controller.hpp"

// Program pozwalajacy sterowac syntezatorem z konsoli
void start_interactive() {
    SynthMqController controller;
    std::string s;
    int r_freq[10] = {0};
    float r_vol[10] = {0.0};
    SynthControlPack scp[10];

    while (true) {
        std::getline(std::cin, s);
        std::cin.clear();

        if (s == "q") {
            controller.send_quit();
            return;
        }

        int packs = std::sscanf(s.c_str(), "%d %f %d %f %d %f %d %f %d %f %d %f %d %f %d %f %d %f %d %f",
            &r_freq[0], &r_vol[0],
            &r_freq[1], &r_vol[1],
            &r_freq[2], &r_vol[2],
            &r_freq[3], &r_vol[3],
            &r_freq[4], &r_vol[4],
            &r_freq[5], &r_vol[5],
            &r_freq[6], &r_vol[6],
            &r_freq[7], &r_vol[7],
            &r_freq[8], &r_vol[8],
            &r_freq[9], &r_vol[9]
        ) / 2;

        for (int i = 0; i < packs; ++i) {
            scp[i].frequency = r_freq[i];
            scp[i].volume = r_vol[i];
        }

        controller.send_control_packs(scp, packs);
    }
}

int main(){
    start_interactive();
}
