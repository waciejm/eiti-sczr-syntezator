#pragma once

#include <cstdio>
#include <cstring>
#include <iostream>

#include <mqueue.h>

// Rozmiar bufora do tworzenia wiadomosci
const int BUFFER_SIZE = 13000;
// nazwa kolejki komuniaktow
const char* MQ_NAME = "/synthesizer_control";
// Maksymalna ilosc wiadomosci w kolejce
const int MQ_MAXMSG = 10;
// Maksymalny rozmiar wiadomosci w kolejce komunikatow
const int MQ_MSGSIZE = 1024;

// Paczka zawierajaca konfiguracje oscylatora
struct SynthControlPack {
    int frequency;
    float volume;

    SynthControlPack(int f = 0, float v = 0.0f) : frequency(f), volume(v) {};
};

// Klasa pozwalajaca na wysyłanie komunikatów do syntezatora
class SynthMqController {
public:

    SynthMqController() {
        // Otwarcie kolejki komunikatów
        mq_attr mqa;
        mqa.mq_maxmsg = MQ_MAXMSG;
        mqa.mq_msgsize = MQ_MSGSIZE;
        message_queue = mq_open(MQ_NAME, O_WRONLY | O_CREAT, S_IRWXU, &mqa);
        if (message_queue < 0) {
            int err = errno;
            std::cerr << "synth_mq_controller: Failed to open mq - " << strerror(err) << std::endl;
        } else {
            std::clog << "synth_mq_controller: Opened message queue - " << MQ_NAME << std::endl;
        }
    }

    ~SynthMqController() {
        mq_close(message_queue);
        mq_unlink(MQ_NAME);
    }

    // Wyslanie paczki punktow kontrolnych
    void send_control_packs(SynthControlPack scp[], int n) {
        n = std::min(n, 10);
        buffer_pointer = 0;
        for (int i = 0; i < n; ++i) {
            buffer_push_int(scp[i].frequency);
            buffer_push_float(scp[i].volume);
        }
        buffer_send();
    }

    // Wyslanie rzadania wylaczenia syntezatora
    void send_quit() {
        buffer_pointer = 0;
        buffer_push((char*)"q", 1);
        buffer_send(1);
    }

private:

    mqd_t message_queue;
    char buffer[BUFFER_SIZE];
    int buffer_pointer = 0;

    // Zapisz int'a w charach
    void transmute_int_to_chars(int i, char c[sizeof(int)]) {
        char* pointer = (char*)(&i);
        for (int i = 0; i < sizeof(int); ++i) {
            c[i] = pointer[i];
        }
    }

    // Zapisz float'a w charach
    void transmute_float_to_chars(float f, char c[sizeof(float)]) {
        char* pointer = (char*)(&f);
        for (int i = 0; i < sizeof(float); ++i) {
            c[i] = pointer[i];
        }
    }

    // Dodaj fragment wiadomosci do bufora
    void buffer_push(char data[], int n) {
        for (int i = 0; i < n; ++i) {
            buffer[buffer_pointer] = data[i];
            ++buffer_pointer;
        }
    }

    // Dodaj int do bufora
    void buffer_push_int(int i) {
        char c[sizeof(int)];
        transmute_int_to_chars(i, c);
        buffer_push(c, sizeof(int));
    }

    // Dodaj float do bufora
    void buffer_push_float(float f) {
        char c[sizeof(float)];
        transmute_float_to_chars(f, c);
        buffer_push(c, sizeof(float));
    }

    // Wyslij zawartosc bufora jako wiadomosc
    void buffer_send(int priority = 0) {
        int e = mq_send(message_queue, buffer, buffer_pointer, priority);
        if (e < 0) {
            int err = errno;
            std::cerr << "synth_mq_controller: Failed to send message - " << strerror(err) << std::endl;
        }
        buffer_pointer = 0;
    }
};
