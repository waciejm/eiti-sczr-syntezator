#include <array>
#include <iostream>
#include <chrono>
#include <fstream>

#include <errno.h>
#include <mqueue.h>

#include <Gamma/AudioIO.h>
#include <Gamma/Oscillator.h>

#include "synth_mq_controller.hpp"

// Maksymalna liczba grajacych oscylatorow
const int N_SYNTHS = 10;

// Czestotliwosci oscylatorow
std::array<int, N_SYNTHS> s_freq = { 0 };
// Glosnosc oscylatorow
std::array<float, N_SYNTHS> s_vol = { 0.0 };
// NuLiczba grajacych oscylatorow
int s_active = 0;

// Klasa generujaca dzwiek z ustawionych oscylatorow
class Synths : public gam::AudioCallback {
public:

    Synths() {
		mAudioIO.append(*this);
		initAudio(44100);
		mAudioIO.start();
	}

    // Callback tworzacy probki dzwieku
	void onAudio(gam::AudioIOData& io) {
        
        // Ustaw odpowiednie czestotliwosci oscylatorow
        for (int i = 0; i < s_active; ++i) {
            oscillators[i].freq(s_freq[i]);
        }


		while(io()){
            float sample = 0.0;

            // Zsumuj probki z aktywnych oscylatorow
            for (int i = 0; i < s_active; ++i) {
                sample += oscillators[i]() * s_vol[i];
            }

            // Zapisz probke
			io.out(0) = sample;
            io.out(1) = sample;
		}
	}

private:
	gam::AudioIO mAudioIO;
    std::array<gam::Sine<>, N_SYNTHS> oscillators = {gam::Sine<>()};

    // Inicjalizacja backendu dziweku
	void initAudio(
		double framesPerSec, unsigned framesPerBuffer=128,
		unsigned outChans=2, unsigned inChans=0
	){
		mAudioIO.framesPerSecond(framesPerSec);
		mAudioIO.framesPerBuffer(framesPerBuffer);
		mAudioIO.channelsOut(outChans);
		mAudioIO.channelsIn(inChans);
		gam::sampleRate(framesPerSec);
	}
};

// Klasa otrzymujaca paczki z kolejki komunikatow
// Aktualizuje ustawienia oscylatorow
class MqReciever {
public:

    MqReciever() : log("log/log_synthesizer", std::ofstream::app) {
        // Otworz kolejke komunikatow
        mq_attr mqa;
        mqa.mq_maxmsg = MQ_MAXMSG;
        mqa.mq_msgsize = MQ_MSGSIZE;
        message_queue = mq_open(MQ_NAME, O_RDONLY | O_CREAT, S_IRWXU, &mqa);
        if (message_queue < 0) {
            int err = errno;
            std::cerr << "synthesizer: Failed to open mq - " << strerror(err) << std::endl;
        } else {
            std::clog << "synthesizer: Opened message queue - " << MQ_NAME << std::endl;
        }
    }

    ~MqReciever() {
        mq_close(message_queue);
        mq_unlink(MQ_NAME);
    }

    void blocking_start() {
        // Poinformuj o starcie
        log_start();
        while (true) {
            // Odbierz wiadomosc do bufora
            ssize_t size = mq_receive(message_queue, buffer, BUFFER_SIZE, nullptr);
            if (size < 0) {
                int err = errno;
                std::cerr << "synthesizer: Failed to receive message - " << strerror(err) << std::endl;
                return;
            }
            // Poinformuj o otrzymaniu nowej paczki
            log_new_pack();

            // Sprawdz czy otrzymano rzadanie zakonczenia pracy
            if (size == 1 && buffer[0] == 'q') {
                return;
            }

            const int s = sizeof(int) + sizeof(float);
            int n = size / s;
  
            // Zaktualizuj ustawienia oscylatorow
            for (int i = 0; i < n; ++i) {

                // Odczytaj czestotliwosc z wiadomosci
                char c_freq[sizeof(int)];
                for (int ic = 0; ic < sizeof(int); ++ic) {
                    c_freq[ic] = buffer[i * s + ic];
                }
                // Zaktualizuj czestotliwosc
                s_freq[i] = transmute_chars_to_int(c_freq);

                // Odczytaj glosnosc z wiadomosci
                char c_vol[sizeof(float)];
                for (int ic = 0; ic < sizeof(float); ++ic) {
                    c_vol[ic] = buffer[i * s + sizeof(int) + ic];
                }
                // Zaktualizuj glosnosc
                s_vol[i] = transmute_chars_to_float(c_vol);
            }
            // Zaktualizuj ilosc aktywnych oscylatorow
            s_active = n;
        }
        log_exit();
    }

private:
    int64_t index = 0;
    std::ofstream log;
    mqd_t message_queue;
    char buffer[BUFFER_SIZE];

    // Zamien chary na int'a
    int transmute_chars_to_int(char c[sizeof(int)]) {
        int* pointer = (int*)c;
        return *pointer;
    }

    // Zamien chary na float'a
    float transmute_chars_to_float(char c[sizeof(float)]) {
        float* pointer = (float*)c;
        return *pointer;
    }

    // Obecny czas
    std::int64_t get_time() {
        return std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now().time_since_epoch()
        ).count();
    }

    // Zapisz rozpoczecie pracy do logu
    void log_start() {
        log << "start " << get_time() << std::endl;
    }

    // Zapisz otrzymanie paczki do logu
    void log_new_pack() {
        log << index << " next_pack " << get_time() << std::endl;
        ++index;
    }

    // Zapisz zakonczenie pracy do logu
    void log_exit() {
        log << "exit " << get_time() << std::endl;
    }
};

int main(){
    Synths synths;
    MqReciever reciever;
    reciever.blocking_start();
}
