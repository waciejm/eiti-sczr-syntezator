#include <iostream>
#include <optional>
#include <vector>
#include <thread>

#include <sched.h>

#include <boost/process.hpp>

#include "../synthesizer/synth_mq_controller.hpp"

using namespace boost::process;

// Mozliwe ustawienia schedulera
enum class Scheduler { RR, FIFO, OTHER };

// Konfiguracja uruchomienia programu
struct Config {
    // Szerokosc obrazu
    int cam_width = 640;
    // Wyskosc obrazu
    int cam_height = 480;
    // Liczba procesow B
    int nprocessors = 1;
    // Czy procesy B maja wyswietlac podglad
    int preview = 0;
    // ID kamery
    int camera_id = 1;
    // Sciezka do pliku wideo
    std::optional<std::string> file = std::nullopt;
    // Ustawienie schedulera procesow A
    Scheduler producer_sched = Scheduler::OTHER;
    // Ustawienie schedulera procesow B
    Scheduler processor_sched = Scheduler::OTHER;
    // Ustawienie schedulera procesow C
    Scheduler synthesizer_sched = Scheduler::OTHER;

    // Sproboj utworzyc konfiguracje z argumentow wywolania programu
    static std::optional<Config> from_args(int argc, char **argv) {
        Config c;
        if (argc % 2 == 0) {
            return std::nullopt;
        }
        for (int i = 1; i < argc; i += 2) {
            if (std::string(argv[i]) == "--width") {
                int width = std::atoi(argv[i+1]);
                if (width <= 0) return std::nullopt;
                c.cam_width = width;
            } else if (std::string(argv[i]) == "--height") {
                int height = std::atoi(argv[i+1]);
                if (height <= 0) return std::nullopt;
                c.cam_height = height;
            } else if (std::string(argv[i]) == "--nprocessors") {
                int nprocessors = std::atoi(argv[i+1]);
                if (nprocessors <= 0) return std::nullopt;
                c.nprocessors = nprocessors;
            } else if (std::string(argv[i]) == "--sched-producer") {
                if (std::string(argv[i+1]) == "rr") {
                    c.producer_sched = Scheduler::RR;
                } else if (std::string(argv[i+1]) == "fifo") {
                    c.producer_sched = Scheduler::FIFO;
                } else {
                    return std::nullopt;
                }
            } else if (std::string(argv[i]) == "--sched-processor") {
                if (std::string(argv[i+1]) == "rr") {
                    c.processor_sched = Scheduler::RR;
                } else if (std::string(argv[i+1]) == "fifo") {
                    c.processor_sched = Scheduler::FIFO;
                } else {
                    return std::nullopt;
                }
            } else if (std::string(argv[i]) == "--sched-synthesizer") {
                if (std::string(argv[i+1]) == "rr") {
                    c.synthesizer_sched = Scheduler::RR;
                } else if (std::string(argv[i+1]) == "fifo") {
                    c.synthesizer_sched = Scheduler::FIFO;
                } else {
                    return std::nullopt;
                }
            } else if (std::string(argv[i]) == "--file") {
                c.file = std::string(argv[i+1]);
            } else if (std::string(argv[i]) == "--camera") {
                int id = std::atoi(argv[i+1]);
                c.camera_id = id;
            } else if (std::string(argv[i]) == "--preview") {
                if (std::string(argv[i+1]) == "on") {
                    c.preview = 1;
                } else if (std::string(argv[i+1]) == "off") {
                    c.preview = 0;
                } else {
                    return std::nullopt;
                }
            } else {
                return std::nullopt;
            }
        }
        return c;
    }
};

// Klasa zarzadcy - proces D
class Manager {
public:

    // uruchom system i zablokuj watek
    void blocking_start(Config& c) {
        // Uruchom proces A
        child producer = startProducer(std::pair<int, int>(c.cam_width, c.cam_height), c.nprocessors + 1, c.producer_sched, c.camera_id, c.file);
        // Uruchom procesy B
        std::vector<child> processors;
        for (int i = 0; i < c.nprocessors; ++i) {
            processors.push_back(
                std::move(startProcessor(std::pair<int, int>(c.cam_width, c.cam_height), c.nprocessors + 1, c.processor_sched, c.preview))
            );
        }
        // Uruchom proces C
        child synthesizer = startSynthesizer(c.synthesizer_sched);

        // Czekaj na wejscie zeby zamknac system
        std::cout << ">>> Press enter to quit <<<" << std::endl;
        std::getchar();
        cleanup();
    }

private:
    opstream producer_ostream;
    std::vector<opstream> processor_ostreams;
    SynthMqController smc;

    // Uruchom proces A
    child startProducer(std::pair<int, int> desired_resolution, int buffers, Scheduler sched, int camera_id, std::optional<std::string> file_path) {
        child c("./bin/producer", std_in < producer_ostream);
        // Wyslij procesowi A konfiguracje
        if (file_path) {
            producer_ostream << 1 << " " << buffers << " " << desired_resolution.first << " " << desired_resolution.second
                             << " " << file_path.value() << std::endl;
        } else {
            producer_ostream << 0 << " " << buffers << " " << desired_resolution.first << " " << desired_resolution.second
                             << " " << camera_id << std::endl;
        }
        setScheduler(c.id(), sched);
        // Spij zeby dac procesowi czas stworzyc dzielony bufor obrazow
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        return std::move(c);
    }

    // Uruchom proces B
    child startProcessor(std::pair<int, int> image_resolution, int buffers, Scheduler sched, int preview) {
        processor_ostreams.emplace_back();
        child c("./bin/processor", std_in < processor_ostreams.back());
        // Wyslij procesowi B konfiguracje
        processor_ostreams.back() << buffers << " " << image_resolution.first << " " << image_resolution.second << " " << preview << std::endl;
        setScheduler(c.id(), sched);
        return std::move(c);
    }

    // Uruchom proces C
    child startSynthesizer(Scheduler sched) {
        child c("./bin/synthesizer");
        setScheduler(c.id(), sched);
        return c;
    }

    // Zamknij system i posprzataj
    void cleanup() {
        for (int i = 0; i < processor_ostreams.size(); ++i) {
            processor_ostreams[i].close();
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(300));

        producer_ostream.close();
        smc.send_quit();
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }

    // Ustaw scheduler procesu
    int setScheduler(pid_t process, Scheduler sched_policy){
        int result;
        sched_param param{99};
        sched_param def_param{};
        switch(sched_policy){
            case Scheduler::RR: {
                result = sched_setscheduler(process, SCHED_RR, &param);
                break;
            }
            case Scheduler::FIFO: {
                result = sched_setscheduler(process, SCHED_FIFO, &param);
                break;
            }
            case Scheduler::OTHER: {
                result = sched_setscheduler(process, SCHED_OTHER, &def_param);
                break;
            }
            default:
                break;
        }
        return result;
    }

    // int setAffinity(pid_t process, cpu_set_t cpu) {
    //     return sched_setaffinity(process, sizeof(cpu_set_t), &cpu);
    // }
};

// Wypisz mozliwe argumenty wywolania
void print_help() {
    std::cout << "Help:\n";
    std::cout << "--width <n>                     <- set camera width\n";
    std::cout << "--height <n>                    <- set camera height\n";
    std::cout << "--nprocessors <n>               <- number of processors\n";
    std::cout << "--sched-producer (rr/fifo)      <- alternative producer scheduler\n";
    std::cout << "--sched-processor (rr/fifo)     <- alternative processor scheduler\n";
    std::cout << "--sched-synthesizer (rr/fifo)   <- alternative processor scheduler\n";
    std::cout << "--file <path>                   <- use video file instead of camera\n";
    std::cout << "--camera <id>                   <- choose camera\n";
    std::cout << "--preview (on/off)              <- show processor preview" << std::endl;
}

int main(int argc, char** argv)
{
    // Stworz config z argumentow wywolania
    std::optional<Config> c = Config::from_args(argc, argv);
    if (c) {
        // Uruchom system jesli udalo sie stworzyc config
        Manager().blocking_start(c.value());
    } else {
        // Wypisz pomoc jesli nie udalo sie stworzyc configu
        print_help();
    }
    return 0;
}
